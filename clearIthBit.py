def clearIthBit(num,i):
    mask=1<<i
    num=num & ~mask
    return num

print(clearIthBit(13,1))
# check
print(bin(13))
print(clearIthBit(13,2))
# check
print(bin(9))