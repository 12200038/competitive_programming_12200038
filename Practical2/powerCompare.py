def compare(a,b,c):
    if(power(a,c)<power(b,c)):
        print("<")
    elif(power(a,c)>power(b,c)):
        print(">")
    else:
        print("=")

def power(a, n):
    # Stores final answer
    ans = 1
    while (n > 0):
        last_bit = (n & 1)
        # Check if current LSB
        # is set
        if (last_bit):
            ans = ans * a
        a = a * a
        # Right shift
        n = n >> 1
    return ans
# Driver code
a = -8
b = 6
c = 3
compare(a,b,c)