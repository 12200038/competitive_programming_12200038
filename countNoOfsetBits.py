def countNoOfsetBits(num):
    count=0
    while(num>0):
        if(num & 1 > 0):
            count+=1
        num=num>>1
    return count
print(countNoOfsetBits(1826))
print(bin(1826))