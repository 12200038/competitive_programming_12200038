# Python3 implementation of the approach

# Function to return the product
# of two complex numbers
def cmul(sq1, sq2):

	ans = [0] * 2

	# For real part
	ans[0] = ((sq1[0] * sq2[0]) -
			(sq1[1] * sq2[1]))

	# For imaginary part
	ans[1] = ((sq1[1] * sq2[0]) +
			sq1[0] * sq2[1])

	return ans

# Function to return the complex
# number raised to the power n
def power(x, n):

	ans = [0] * 2
	if (n == 0):
		ans[0] = 0
		ans[1] = 0
		return ans
	
	if (n == 1):
		return x

	# Recursive call for n/2
	sq = power(x, n // 2)
	if (n % 2 == 0):
		return cmul(sq, sq)
	return cmul(x, cmul(sq, sq))

# Driver code
if __name__ == "__main__":

	x = [0] * 2

	# Real part of the complex number
	x[0] = 18

	# Imaginary part of the
	# complex number
	x[1] = -13
	n = 8

	# Calculate and print the result
	a = power(x, n)
	print(a[0], " + i ( ", a[1], " )")

# This code is contributed by ita_c
