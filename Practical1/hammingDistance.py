from operator import xor


class HammingDistance:
    def func(x,y):
        xor_value=x^y
        count=0
        
        while xor_value!=0:
            count+=1
            xor_value &= (xor_value-1)
        return count
    
    print(func(1,4))