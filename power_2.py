def powertwo(number):
    x = number & (number-1)
    if(x == 0):
        return f"{number} is power of 2"
    return f"{number} is not power of 2"
    
print(powertwo(34))
print(powertwo(3))
print(powertwo(2))