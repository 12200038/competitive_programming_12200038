def ithBit(n, k):
    print((n & (1 << (k - 1))) >> (k - 1))
if __name__ == '__main__':
    n = 13
    k = 2
    ithBit(n, k)